package com.nespresso.sofa.interview.cart.services;

import com.nespresso.sofa.interview.cart.model.Cart;

import java.util.HashMap;
import java.util.Map;

/**
 * If one or more product with code "1000" is purchase, ONE product with code 9000 is offer
 * For each 10 products purchased a gift with code 7000 is offer.
 */
public class PromotionEngine {

    public static final String PROMOTION = "9000";
    public static final String PRODUCT_WITH_PROMOTION = "1000";
    public static final String GIFT = "7000";


    public Cart apply(Cart cart) {

        Map<String, Integer> products = removePromotionProducts(cart.getProducts());
        products = removeGiftProducts(products);
        products = addPromotionProduct(products);
        products = addGiftProduct(products);

        return new Cart(cart.getId(), products);
    }


    private Map<String, Integer> addGiftProduct(Map<String, Integer> allProducts) {

        int giftQuantity = 0;
        for (String code : allProducts.keySet()) {
            int quantity = allProducts.get(code);
            giftQuantity += quantity / 10;
        }
        Map<String, Integer> products = new HashMap<>(allProducts);

        if (giftQuantity > 0) {
            products.put(GIFT, giftQuantity);
        }

        return products;
    }


    private Map<String, Integer> addPromotionProduct(Map<String, Integer> allProducts) {


        Map<String, Integer> products = new HashMap<>(allProducts);
        for (String code : allProducts.keySet()) {
            if (code.equals(PRODUCT_WITH_PROMOTION) && notContainProduct(allProducts, PROMOTION)) {
                products.put(PROMOTION, 1);
            }
        }
        return products;
    }

    private boolean notContainProduct(Map<String, Integer> allProducts, String product) {

        for (String code : allProducts.keySet()) {
            if (product.equals(code)) {
                return false;
            }
        }
        return true;

    }


    private Map<String, Integer> removePromotionProducts(Map<String, Integer> allProducts) {

        Map<String, Integer> products = new HashMap<>();
        for (String code : allProducts.keySet()) {
            if (!code.equals(PROMOTION)) {
                products.put(code, allProducts.get(code));
            }
        }

        return products;
    }

    private Map<String, Integer> removeGiftProducts(Map<String, Integer> allProducts) {

        Map<String, Integer> products = new HashMap<>();
        for (String code : allProducts.keySet()) {
            if (!code.equals(GIFT)) {
                products.put(code, allProducts.get(code));
            }
        }

        return products;

    }
}
