package com.nespresso.sofa.interview.cart.presenter;

import java.util.Map;
import java.util.UUID;

public interface Presenter {

    String present(UUID id, Map<String, Integer> products);

}
