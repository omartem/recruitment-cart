package com.nespresso.sofa.interview.cart.services;

import com.nespresso.sofa.interview.cart.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class CartService {

    @Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private CartStorage cartStorage;

    /**
     * Add a quantity of a product to the cart and store the cart
     *
     * @param cartId      The cart ID
     * @param productCode The product code
     * @param quantity    Quantity must be added
     * @return True if card has been modified
     */
    public boolean add(UUID cartId, String productCode, int quantity) {
        final Cart cart = cartStorage.loadCart(cartId);

        if (quantity > 0) {
            Map<String, Integer> products = new HashMap<>(cart.getProducts());
            int oldQuantity = getQuantityOfProduct(productCode, cart);
            products.put(productCode, quantity + oldQuantity);
            cartStorage.saveCart(new Cart(cartId, products));
            return true;
        } else {
            return false;
        }

    }

    /**
     * Set a quantity of a product to the cart and store the cart
     *
     * @param cartId      The cart ID
     * @param productCode The product code
     * @param quantity    The new quantity
     * @return True if card has been modified
     */
    public boolean set(UUID cartId, String productCode, int quantity) {
        final Cart cart = cartStorage.loadCart(cartId);

        Map<String, Integer> products = cart.getProducts();

        if (isInProducts(products, productCode, quantity)) {
            return false;
        } else {
            products.put(productCode, quantity);
            products = removeEmptyProducts(products);
            cartStorage.saveCart(new Cart(cart.getId(), products));
            return true;
        }

    }


    /**
     * Return the card with the corresponding ID
     *
     * @param cartId
     * @return
     */
    public Cart get(UUID cartId) {
        return cartStorage.loadCart(cartId);
    }

    private Map<String, Integer> removeEmptyProducts(Map<String, Integer> allProducts) {
        Map<String, Integer> products = new HashMap<>();

        for (String code : allProducts.keySet()) {
            if (allProducts.get(code) > 0) {
                products.put(code, allProducts.get(code));
            }
        }
        return products;
    }

    private int getQuantityOfProduct(String productCode, Cart cart) {
        Integer quantity = cart.getProducts().get(productCode);
        if (quantity != null) {
            return quantity;
        } else {
            return 0;
        }
    }

    private boolean isInProducts(Map<String, Integer> allProducts, String productCode, Integer quantity) {
        if (allProducts.containsKey(productCode)) {
            return quantity == allProducts.get(productCode);
        } else {
            return false;
        }
    }
}
