package com.nespresso.sofa.interview.cart.presenter;

import java.util.Map;
import java.util.UUID;

public class CartPresenter implements Presenter {


    @Override
    public String present(UUID id, Map<String, Integer> products) {

        StringBuilder productsOutputBuilder = new StringBuilder();

        for (String product : products.keySet()) {
            productsOutputBuilder.append(product + "=" + products.get(product));
        }

        return "Cart {id: " + id.toString() + ", products: {" + productsOutputBuilder.toString() + "}}";
    }


}
