package com.nespresso.sofa.interview.cart.model;

import com.nespresso.sofa.interview.cart.presenter.CartPresenter;
import com.nespresso.sofa.interview.cart.presenter.Presenter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.UUID.randomUUID;

public final class Cart implements Serializable {

    private final UUID id;
    private Map<String, Integer> products = new HashMap<>();

    public Cart() {
        this(randomUUID());
    }

    public Cart(UUID id) {
        this.id = id;
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        this.products = products;
    }

    public Cart(UUID id, Map<String, Integer> products) {
        this.id = id;
        this.products = products;
    }

    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return products;
    }


    @Override
    public String toString() {
        Presenter presenter = new CartPresenter();
        return presenter.present(id, products);
    }


}
